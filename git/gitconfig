[include]
    path = ~/.gitconfig.local

[core]
    editor = vim
    excludesfile = ~/.gitignore

[color "branch"]
    current = green bold
    local = green
    remote = yellow

[color "diff"]
    frag = cyan bold
    meta = yellow bold
    new = green
    old = red

[diff "bin"]
    textconv = hexdump -v -C

[push]
    default = current

[alias]
    a = add
    ap = add --patch

    pu = push
    puf = push --force
    # Push current branch to origin and track upstream
    pub = "!git push -u origin $(git symbolic-ref --short HEAD)"

    pl = pull
    plr = pull --rebase

    c = commit
    ca = commit --amend
    can = commit --amend --no-edit

    co = checkout
    cob = checkout -b

    d = diff
    dc = diff --cached
    # Show diff between current state and $1 commits ago
    di = "!d() { git diff --patch-with-stat HEAD~$1; }; \
           git diff-index --quiet HEAD -- || clear; d"

    s = status

    st = stash
    stp = stash pop
    stl = stash list

    l = log --pretty=oneline --graph --abbrev-commit
    sh = shortlog --summary --numbered
    fl = log --patch

    b = branch
    bd = branch --delete
    bdf = branch --delete --force

    cp = cherry-pick
    ri = rebase --interactive

    un = reset HEAD --

    g = grep
    gci = grep --count --ignore-case

# vim: set filetype=gitconfig :
